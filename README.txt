This sample shows a Java SOAP client that can create issues in JIRA. 

For more information about this sample, see the tutorial at: 

https://developer.atlassian.com/display/JIRADEV/Creating+a+JIRA+SOAP+Client 
